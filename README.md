## 프로젝트 일정

1. 모델 프로토타입 구현 (5/1 ~ 5/21)
- 1.1 RAG 파이프라인 결정
- 1.2 RAG 테스트 데이터 관련 작업 (preprocess function 구현 필요)
    - .txt, .md, .pdf, .html등 extension에 따라 noise 제거 작업  
    - https://faraazmohdkhan.medium.com/preprocessing-of-content-for-a-rag-application-ac720696bacc
    - https://faraazmohdkhan.medium.com/best-techniques-for-chunking-for-a-rag-application-f05ec558d683
    - base llm model fine tuning 작업 보단 rag 모듈을 위한 데이터 preprocess 작업이 더 효율적
- 1.3 retriever 및 llm 모델 결정
    - retriever
        - AutoMergingRetriever, FAISS etc...
    - llm
        - 온라인: gpt-3.5.-turbo
        - 오프라인: 메타 llama3 (최근 발표) 
- 1.4 groundness, context relevance, accuracy, human evaluation등 성능 metrics 결정 및 관련 기능 구현 필요
- 1.5 

2. 모델 성능 테스트 및 튜닝 작업 (5/22 ~ 6/7)
- 2.1 context-aware chunking  


3. UI 적용 및 테스트 (6/8 ~ 6/21)

## 환경 설정 방법

파이썬에서는 한 라이브러리에 대해 하나의 버전만 설치가 가능합니다.

여러개의 프로젝트를 진행하게 되면 이는 문제가 됩니다. 작업을 바꿀때마다 다른 버전의 라이브러리를 설치해야합니다.

이를 방지하기 위한 격리된 독립적인 가상환경을 제공합니다.

일반적으로 프로젝트마다 다른 하나의 가상환경을 생성한 후 작업을 시작하게 됩니다.

가상환경의 대표적인 모듈은 3가지가 있습니다.

venv : Python 3.3 버전 이후 부터 기본모듈에 포함됨

virtualenv : Python 2 버전부터 사용해오던 가상환경 라이브러리, Python 3에서도 사용가능

conda : Anaconda Python을 설치했을 시 사용할 수있는 모듈

pyenv : pyenv의 경우 Python Version Manger임과 동시에 가상환경 기능을 플러그인 형태로 제공

#### 방법 1. Python 설치

cmd에서 python 입력 후 엔터 눌렀을때 Microsoft Store에서 설치 창이 뜨면 설치치


**※ Add PATH 체크박스 선택**

##### 1.1 python 및 pip3 설치
```bash  
python --version
python -m ensurepip --upgrade
```

##### 1.2 python 가상환경 설정 및 activate

가상환경과 jupyter notebook kernel 연결

```bash 
pip3 install virtualenv
virtualenv <가상환경명> or python -m venv <가상환경명>
source <가상환경명>/Scripts/activate
pip3 install jupyter
python -m ipykernel install --user --name <가상환경명> --display-name <가상환경명>
jupyter notebook
```

필요한 라이브러리들은 pip3로 설치하면 된다.
```bash
pip3 install python-dotenv langchain_openai
```

##### 1.3 가상환경 deactivate
```bash 
deactivate
```

#### 방법 2. [Download Anaconda](https://www.anaconda.com/download)

## Basic RAG Architecture

## Advanced RAG Architecture

## Modular RAG Architecture


## RAG Performance Metrics
1. Evaluation in Isolation (Retrieval)

2. Evaluation E2E

## 참고 자료


