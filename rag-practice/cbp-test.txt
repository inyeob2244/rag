금리 산출
1. 개요
금리란 고객이 금융기관에 일정기간 동안 금액을 예치 혹은 대여한 대가로 받거나 지불해야 하는 이자의 비율을 말하는데 이자계산의 핵심 계산요소로 사용된다.

금리산출 기능은 CBP에서 Service BO로 정의하는 비지니스 오브젝트(ArrIntRtProvider)가 담당한다.

2. 금리산출 관련 업무개념
2.1. 금리 변동 방식
금리는 계좌의 금리적용방법에 따라 세 가지 변동방식이 존재한다.

2.1.1. 고정금리
고정금리
특징 : 대출 실행시 결정된 금리가 대출 만기까지 동일하게 유지

장점 : 시장금리 상승기에 금리 인상이 없음. 대출기간 중 월이자액이 균일하여 상환계획 수립 용이

단점 : 시장금리 하락기에 금리 인하 효과가 없어 변동금리보다 불리. 통상 대출 시점에는 변동금리보다 금리가 높음

2.1.2. 변동금리
변동금리
특징 : 일정 주기(3/6/12개월 등)마다 대출 기준금리의 변동에 따라 대출금리 변동※

장점 : 시장금리 하락기에는 이자 부담 경감 가능. 통상 대출 시점에는 고정금리 방식보다 금리가 낮음

단점 : 시장금리 상승시 이자 부담이 증가될 수 있음

변동금리 적용 예시 : 금리재산정 주기가 3개월인 변동금리 대출의 경우, 당초 계약 당시 2.0%였던 대출 기준금리가
3개월 후 2.5fh 0.5% 상승하였다면 대출금리도 이에 따라 0.5% 상승

변동금리 예시
2.1.3. 혼합금리
혼합금리
특징 : 고정금리 방식과 변동금리 방식이 결합된 형태(통상 일정기간 고정금리 적용 후 변동금리 적용)

장점 : 금융소비자의 자금계획에 맞춰 운용 가능

2.2. 금리체계
금리체계는 금융기관이 금리를 결정하기 위한 구조로 금리요소와 금리요소간의 산식으로 구성되었다.

Table 1. 수신주요금리체계
금리체계명

구성요소

설명

중도해지금리

중도해지금리

만기일 이전 해지 시 적용되는 금리

정상금리

약정이율, 우대금리

만기시점에 적용되는 금리

만기후금리

만기후이율

만기이후 해지 시 만기시점부터 해지일까지 적용되는 금리

Table 2. 여신주요금리체계
금리체계명

구성요소

설명

약정금리

기준금리, 가산금리 , 우대금리

약정된 기간동안 대출한 결과로 받기로 약속된 정상금리

연체금리

원금연체금리, 이자연체금리

연체로 인한 패널티 금리로 약정금리에 가산되는 금리

2.3. 금리요소
금리체계를 구성하는 각각의 금리요소는 상품조건으로 정의되어 금리산출에 필요한 속성정보들이 정의되어 있다.

금리조건
Figure 1. 상품금리조건
2.3.1. 금리산출기준 속성
조회기준일
상품시스템에는 금융기관에서 고시한 금리의 변경이력이 관리된다. 조회기준일 속성은 상품에 등록된 금리의 변경정보를 계약의 어떠한 일자를 기준으로 조회할 지의 기준으로 사용된다. 상품에 정의된 조회기준일 종류별 의미는 다음과 같다.

기준일유형	설명
신규일

계약의 신규일을 의미하며, 상품에 고시된 금리의 변경 이력 중 계약의 신규 시점에 고시된 금리산출 기준을 이용하여 금리를 조회하는 용도로 사용한다.

만기일

계약의 만기일을 의미하며, 금리에서 만기일을 사용하는 경우는 계약시점의 금리가 아닌 만기시점의 상품에 고시된 금리를 이용하여 금리를 산출할 때이며 만기후 금리나, 우대금리 중에 만기 해지 시점의 우대금리 등이 조회기준일을 만기일로 설정된다.

거래일

금리산출 시점을 의미하며, 금리산출을 위한 조회시작일을 기준으로 고시된 금리변경이력을 조회한다.

금리적용방식
금융 상품의 특성에 따라 다양한 금리에 대한 변동 유형을 의미하며 금리적용방식에 따라 추가적인 속성 기준으로 금리를 변동 적용한다. (참조 : 금리변동성에 따른 분류)

구분	설명
고정 적용

약정 시점에 확정된 금리를 변동없이 만기까지 적용하는 방식

변동 적용

약정 이후에도 주기적으로 변동된 금리를 적용하는 방식

고정 후 변동

약정 직후 특정 기간 동안 변동없이 적용하였다가, 이후 주기적으로 변동하는 방식

타상품 참조

금리의 변동여부를 해동 조건에서 결정하지 않고, 다른 금리조건을 무조건 참조하는 방식

변동정보 적용방식
금리적용방식이 변동 적용/ 고정 후 변동 적용일 경우 변동구간에 대한 변동 적용방식을 말한다.

구분	설명
신규 이후 변동적용

변동주기 없이, 계약이 신규된 이후 발생한 모든 금리변동을 적용하는 방식

월 주기 변동적용

월 단위 변동주기에 따라 변동된 금리를 적용하는 방식

일 주기 변동적용

일 단위 변동주기에 따라 변동된 금리를 적용하는 방식

약정주기 변동적용

변동주기를 다른 조건으로 정의하여 해당 조건값에 따라 금리변동을 적용하는 방식

금리데이터유형코드
금리값(데이터) 관리방식을 말하며, 관리방식에 따라 다음의 속성을 사용한다.

금리데이터유형	속성	속성설명
금리값

해당 조건에 정의된 조건값에 따라 금리값을 적용하는 방식

최소/최대/기본

결정된 금리

기준금리연동

기준금리에 따라 금리값을 적용하는 방식

기준금리종류

참조 대상이 되는 기준금리 종류

적용시점

기준금리 조회 시점

타상품금리산출

다른 금리조건에 정의된 금리값을 참조하여 해당 금리조건의 금리값을 적용하는 방식

상품코드

연동대상이 되는 상품코드

조건코드

연동대상이 되는 금리조건코드

참조조건/조건값/측정단위

참조대상 조건이 복합조건인 경우 조건을 결정하기 위한 구성조건 및 구성조건 값

참조금리조회시점

참조대상 상품의 금리를 조회하기 위한 기준일

조건값적용방법

구분	설명
상품조건값사용

상품에 정의된 조건값 사용

내부로직산출

특정 업무로직에 의해 조건값이 결정될 경우 , 상품에 조건값을 정의하지 않고 해당 조건값을 산출하는 조건클래스를 구현하여 조건값을 리턴하도록 한다.
참조 : 내부로직산출에 의한 조건값 정의 예시

3. 금리 산출 case study
금리산출/조회는 크게 아래 순서로 처리된다

1).금리산출 대상 계약의 금리체계 조회

2).금리체계 구성요소별 금리값 결정

3).적용금리 계산

4).산출결과 저장

다음은 여러 케이스를 통해 금리산출과정(위 과정)을 살펴본다.

3.1. case 1 수신정기예금 약정금리 생성
3.2. case 2 여신신용대출 약정금리 생성
3.3. case 3 여신신용대출 약정금리 일부요소 재산출
3.4. case 4 여신 신용대출 약정금리 재산출
4. 금리 조회/산출 주요 기능
금리기능은 동일한 핵심처리 모듈에 서로 다른 파라미터 전달을 통해 산출/조회/일부 금리 재산출 등 기능을 제공한다.

주요기능
Table 3. 주요 기능 별 파라미터 매핑
기능\속성

isGenerate

intClctYn

baseRateCreateYn

reCalcnYn

generateArrInterestRateCore

true

N

true

Y

getArrRate

false

Y

N/A

N/A

rebuild

true

N

true

N/A

4.1. 주요 API
클래스

분류

API

설명

ArrIntRtProvider

금리조회

getListArrIntRt

계약의 적용금리 조회

getArrRate (only V4)

계약금리 조회

getListArrIntRtOnAgrmntDt

계약의 약점시점을 기준으로 입력된 금리체계의 금리 산출

getListArrIntRtForIntCalculation

이자계산을 위한 적용금리 조회

getArrRateByIntRtStrctrVrtn

조회기간내 연체금리 산출

getListConfirmedIntRtHistory

계약의 확정금리 이력 조회

getListConfirmedIntRtHistory

계약에 적용된 수신 정상금리 목록 조회

getArrLoanInterestRate

계약에 적용된 여신 정상금리 조회

금리산출

generateArrIntRt

계약금리 산출(계약신규 시 금리생성, 증액/기간연장 시 금리 재산출)

generateArrLoanInterestRate

여신 정상금리 산출

generateArrDepositInterestRate

수신 정상금리 산출

시뮬레이션

simulateArrInterestRateCalculation

금리시뮬레이션 조회

ArrReal

금리조회

getCurrentArrIntRt

계약의 현시점 금리조회

금리재산정

rebuild

특정 혹은 지정 금리요소 재산정

getNextRebuildDate

다음 재산정일 조회

4.2. 주요 클래스
4.2.1. 클래스 기능
클래스유형

클래스명

기능 설명

INTERFACE

ArrIntRtProvider

서비스에 금리 조회/산출 API를 제공하는 인터페이스

SERVICE_BO

ArrIntRtProviderImpl

서비스에 금리조호/산출 API를 제공하는 인터페이스를 구현한 클래스

SERVICE_BO

ArrIntRtCoreProviderImpl

코어 구현체

INTERFACE

ArrIntRtGenerator

계약 내부적으로 금리조회/산출 기능을 제공하는 인터페이스

Service_BO

ArrIntRtGeneratorNormalImpl

금리산출 시 상품레벨조건 관련 처리를 위한 클래스

Service_BO

ArrIntRtGeneratorSuperImpl

ArrIntRtGenerator 인터페이스를 구현한 클래스로 금리조회/산출 기능 처리로직을 구현한 클래스

Biz Class

ArrIntRtGeneratorSuperAgencyArrangementLevel

계약레벨 조건의 금리산출 관련 공통 기능 제공

Biz Class

ArrIntRtGeneratorSuperAgencyGenerateVariableRate

변동금리 산출을 위한 공통기능

Biz Class

ArrIntRtGeneratorSuperAgencyCommon

금리산출을 위한 공통기능 제공

4.3. 주요 처리흐름
금리산출/조회의 주요처리흐름 시퀀스 다이어그램 및 주요처리기능 설명은 다음과 같다.

4.3.1. 시퀀스 다이어그램
ArrIntRtProviderImpl
generateArrIntRt
ArrIntRtProviderImpl
ArrIntRtCoreProviderImpl
_getCnfrmtnIntRt
getConfirmedIntRt
_generateInterestRate
_checkInput
generateIntRt
return
ArrIntRtCoreProviderImpl
ArrIntRtGeneratorSuperImpl
_checkInputAndBuild
getPdInterestRateStructure
_getListIntRtBaseHst
_generateIntRt
_changeIntRtBaseInfo
_createIntermediateRates
_generateArrInterestRateList
_saveTimeRate
ArrIntRtGeneratorSuperImpl
return
PdInterestRateStructureMngr
PdInterestRateStructureMngr
4.3.2. 처리 기능 설명
순서	클래스	메소드	설명	관련 테이블
1

Provider

generateArrIntRt

금리생성기능을 처리하기 위한 주요처리로직은
코어에 구현되어 있으며 본 기능은 코어의 생성을 기능을 호출한다.

1.1

Provider

_getCnfrmtnIntRt

기 확정되어 재산출이 필요 없는 적용금리조회(확정금리 조회)

ar_arr_aply_int_rt_val_d :계약적용금리값명세

ar_arr_aply_int_rt_val_dtl_d: 계약적용금리값명세

1.2

Provider

_generateInterestRate

1.2.1 입력검증 및 금리산출에 필요한 파라미터 설정

1.2.2 금리산출

1.2.2

GeneratorSuper

generateIntRt

금리산출

1.2.2.1

GeneratorSuper

_checkInputAndBuild

입력검증 & 금리산출 필요 파라미터 설정

1.2.2.2

IntStructureMngr

getPdInterestRateStructure

금리체계 조회

1.2.2.3

GeneratorSuper

_generateIntRt

금리산출

1.2.2.3.1

GeneratorSuper

_changeIntRtBaseInfo

금리재산출 인 경우(isGenerate == true),
적용금리기준정보 (금리산출일, 산식조회기준일, 산식내용 등)갱신

ar_arr_aply_int_rt_d: 계약적용금리명세

1.2.2.3.2

GeneratorSuper

_createIntermediateRates

금리체계구성요소별 조건값 결정 (조회구간내 계약조건값이력/상품조건변경이력 포함)

1.2.2.3.3

GeneratorSuper

_generateArrInterestRateList

금리체계구성요소별 조건값을 통한 최종 적용금리 산출

1.2.2.3.4

GeneratorSuper

_saveTimeRate

산출된 적용금리 저장

ar_arr_aply_int_rt_val_d :계약적용금리값명세

ar_arr_aply_int_rt_val_dtl_d: 계약적용금리값명세

5. 금리 산출 내부기능
5.1. 금리산출을 위한 내부 변수
변수 값에 따른 내부 처리 설명

변수

설명

isGenerate

금리생성 여부에 대한 값으로

intClcltYn

이자계산여부에 대한 값으로 'Y’일 경우 금리 조회 기능으로 , 생성에 대한 기능을 수행하지 않음

baseRateCreateYn

기준금리 생성 여부에 대한 값으로 조회 관련 기능일 경우 해당 값이 "N"으로 설정되고 계약레벨로 적재된 값을 조회함.

"Y" 일 경우 기준금리 생성 관련 기능으로 상품레벨로 산정한다.

reCalcnYn

금리 재산출여부에 대한 값으로 해당 값이 "Y"일 경우 기존에 확정된 적용금리가 존재할지라도 무시하고 다시 산출한다.

5.2. 금리산출 관련 기준일자유형
기준일속성

설명

계산방법

startDt

생성 / 조회 구간 시작일

* startDt >= endDt X
* isGenerate일 경우, startDt → 계약객체 맴버변수(ArrIntRtCalcnDt)에 설정

formulaBaseDt

산식조회 기준일

* API로부터 입력받은 산식조회기준일
* 미입력 시 , 최종약정신규일(arrLstAgrmntAndOpnDt)
* 약정신규일 없거나 isGenerate=Y, formulaBaseDt = startDt

calcnBaseStartDt

기준금리 변동주기 시작일

cmptnDt = calcnBaseStartDt

5.3. 금리조건 설정 조합 및 단위 기능
클래스/메소드 명칭 매핑

클래스/메소드구분

full name

abbreviation name

클래스

ArrIntRtGeneratorSuperAgencyArrangementLevel

ArrLvl

클래스

ArrIntRtGeneratorSuperAgencyGenerateVariableRate

Variable

클래스

ArrIntRtGeneratorNormalImpl

Normal

메소드

_generateIntRtForOneCnd_ArrLvlFixedSmplIntRtVal

ArrLvlFixedSmplIntRtVal

메소드

_generateIntRtForOneCnd_ArrLvlFixedRefBaseIntRt

ArrLvlFixedRefBaseIntRt

메소드

_generateIntRtForOneCnd_PdLvlFixedSmplIntRtVal

PdLvlFixedSmplIntRtVal

메소드

_generateIntRtForOneCnd_PdLvlFixedRefBaseIntRt

PdLvlFixedRefBaseIntRt

메소드

_generateIntRtForOneCnd_PdLvlFixedRefOtherPdIntRt

PdLvlFixedRefOtherPdIntRt

메소드

_generateIntForOneCnd_PdLvlCalcByLogic

PdLvlCalcByLogic

위 매핑표를 통해 아래 설명 중 클래스와 메소드의 실제 명칭을 확인할 수 있다.

계약/상품	고정/변동구분	금리데이터유형	클래스	메소드	설명
계약

고정

단순금리값

ArrLvl

ArrLvlFixedSmplIntRtVal

적용금리에 우수리적용

해당 값을 적용금리로 사용

계약

고정

기준금리연동

ArrLvl

ArrLvlFixedRefBaseIntRt

기준금리 조회

조회된 기준금리목록 중 첫번째 기준금리에 스프레드 적용

해당 값을 적용금리로 사용

계약

고정

타상품금리참조

N/A

N/A

상품에서 설정 가능하나 업무요건에 적합하지 않는 기능으로 CBP에서 지원하지 않음

계약

변동

기준금리연동

Variable

generateVariableRate

계약

변동

금리값

N/A

N/A

CBP 지원하지 않음

계약

변동

타상품참조

N/A

N/A

CBP 지원하지 않음

상품레벨&단순조건

?

내부로직산출

Normal

PdLvlCalcByLogic

조건값에 상품에 정의되어 있지 않고 내부로직에 의해 산출이 되어야 한다.

상품레벨&단순조건

고정

단순금리값

Normal

PdLvlFixedSmplIntRtVal

상품에 정의된 고정값에 우수리적용 후 결과 리턴

상품레벨&단순조건

고정

기준금리연동

Normal

PdLvlFixedRefBaseIntRt

고시된 기준금리를 pd_base_int_m에서 관리하고 있음. 따라서 해당 테이블로부터 관련 유형의 특정시점 기준금리 조회 후 우수리 적용하여 결과 리턴

상품레벨&단순조건

고정

타상품금리 참조

Normal

PdLvlFixedRefOtherPdIntRt

참조대상금리조건이 계약에 존재 시 계약조건의 적용금리 사용

참조대상조건이 계약에 미 존재 시 상품조건정보를 기준으로 금리 산출

산출된 값에 우수리 적용 후 결과 리턴

상품레벨&단순조건

변동

금리값 & 기준금리연동

Variable

generateVariableRate

6. 기타
6.1. 확정금리 판단 기준
확정금리란 말그대로 이미 확정된 금리로 금리조회 시 해당 계약에 적용된 확정금리가 존재할 경우
금리를 다시 산출하지 않고 확정된 금리를 그대로 사용한다.
* 적용금리의 확정여부는 모든 금리구성요소가 모두 "확정"일 경우 확정으로 결정된다.

Table 4. 금리구성요소 확정 판단 기준
NO.	기준	확정/미확정	비고
CASE1

금리체계구성요소의 상품조건이 존재하지 않거나 유효하지 않을 경우

확정

목조건(pdCnd==null)일 경우 이에 해당

CASE2

금리체계 금리요소가 금리유형 조건이 아닐 경우

확정

CASE3

우대금리이면서 금리적용시점이 "만기시점" 일 경우

미확정

만기시점에 금리값 조회 필요

CASE4

금리적용방식이 "변동"이면서 rebuild(재산정) 대상이 아닐 경우

미확정

CASE5

복합조건이지만 계층적용방법이 "특정계층적용"이 아닐 경우

미확정

"다계층적용"은 금리값 확정할 수 없음

CASE6

기타

확정

고정금리는 확정 (약정시 적용된 금리를 만기까지 그대로 적용)

6.2. 다음 재산정일자 산출
계약의 다음재산정일 산출 :
1). 금리구성요소 중 리빌드 대상이 여러개 있을 경우, 모든 리빌드 대상에 대해 다음리빌드일자를 구하여
그 중 제일 작은 리빌드 일자를 계약의 리빌드 일자로 리턴한다.
2). 리빌드 대상이 존재하지 않거나, 기준금리 고정인 경우 max_dt(99991231)을 리턴한다.

특정 조건의 다음재산정일자 산출 : 재산정 대상인 조건에 대해서는 ArrCndActionRebuild 인터페이스 구현을 통해
다음재산정일자 산출 기능을 재정의한다. 다음재선정일자는 재산정 대상 조건의 업무요건에 따라 다르게 산출되며
해당 산출 로직에 의해 다음재산정일자를 산출하여 리턴한다.